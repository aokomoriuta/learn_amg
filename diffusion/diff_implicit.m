clear

n=100;

r = 8.0;
for i=1:1:n
  x(i) = 1;
end
x(1) = 10;
for i=1:1:(n/4)
  x(n/2-i) = 8;
  x(n/2+i) = -6;
end
x(n) = -7;
plot(x); grid "on";
print("diff_implicit_0000.png", "-dpng")

for t=1:1:20
  for inT=1:1:1
    # 定数ベクトルb
    for i=1:1:n
      b(i) = x(i);
    end
    
    # 係数行列A
    A = 0;
    A(1,1) = 1;
    A(n,n) = 1;
    for i=2:1:(n-1)
      A(i, i-1) = -r;
      A(i, i) = 2*r+1;
      A(i, i+1) = -r;
    end
    
    # 求解
    invA = inv(A);
    x_n = (invA * b')';
    
    #結果の代入
    x = x_n;
  end
  plot(x); grid "on";
  filename = sprintf("diff_implicit_%04d.png", t);
  print(filename, "-dpng")
end

