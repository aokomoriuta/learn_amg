clear

global size=21;
dt=20.0;
dx=1.0;
r = dt/dx/dx;
T = 20.0;

function idx=idx(i, j)
  global size;
  idx=size*(i-1)+j;
end

function output(x, t)
  global size;
  [ii, jj] = meshgrid (0:1:(size-1), 0:1:(size-1));
  for i=1:1:size
    for j=1:1:size
      data(i, j) = x(idx(i, j));
    end
  end
  mesh (ii, jj, data);
  filename = sprintf("diff2d_implicit_%04d.png", t);
  print(filename, "-dpng");
end

n = size*size;
x = zeros(1, n);
for i=1:1:size
  y = (i-1)/(size-1);
  x(idx(i, 1))    = 14*y-4;
  x(idx(i, size)) = 6*y+4;
  x(idx(1, i))    = -4 * cos(pi*y);
  x(idx(size, i)) = 10 * (1-sin(pi*y));
end
output(x, 0);

tic();
  
# 係数行列A
A = eye(n, n);
for i=2:1:(size-1)
  for j=2:1:(size-1)
    ii = idx(i, j);
    A(ii, ii) = 4*r+1;
    A(ii, idx(i-1, j)) = -r;
    A(ii, idx(i+1, j)) = -r;
    A(ii, idx(i, j-1)) = -r;
    A(ii, idx(i, j+1)) = -r;
  end
end
# 逆行列
invA = inv(A);

for t=1:1:(T/dt)  
  x_next = (invA * x')';
  x = x_next;
end
toc()
output(x, t);
