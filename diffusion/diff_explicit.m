clear

n=100;

r = 0.4;
for i=1:1:n
  x(i) = 1;
end
x(1) = 10;
for i=1:1:(n/4)
  x(n/2-i) = 8;
  x(n/2+i) = -6;
end
x(n) = -7;
plot(x); grid "on";
print("diff_explicit_0000.png", "-dpng")

for t=1:1:20
  for inT=1:1:20
    for i=2:1:(n-1)
      x_n(i) = r*x(i-1) + (1-2*r)*x(i) + r*x(i+1);
    end
    for i=2:1:(n-1)
      x(i) = x_n(i);
    end
  end
  plot(x); grid "on";
  filename = sprintf("diff_explicit_%04d.png", t);
  print(filename, "-dpng")
end

