clear

global n=21;
dt=0.25;
dx=1.0;
r = dt/dx/dx;
T = 20.0;

function idx=idx(i, j)
  global n;
  idx=n*(i-1)+j;
end

function output(x, t)
  global n;
  [ii, jj] = meshgrid (0:1:(n-1), 0:1:(n-1));
  for i=1:1:n
    for j=1:1:n
      data(i, j) = x(idx(i, j));
    end
  end
  mesh (ii, jj, data);
  filename = sprintf("diff2d_explicit_%04d.png", t);
  print(filename, "-dpng");
end

x = zeros(n, 1);
for i=1:1:n
  y = (i-1)/(n-1);
  x(idx(i, 1)) = 14*y-4;
  x(idx(i, n)) = 6*y+4;
  x(idx(1, i)) = -4 * cos(pi*y);
  x(idx(n, i)) = 10 * (1-sin(pi*y));
end
output(x, 0);

tic();
x_n = x;
for t=1:1:(T/dt)
  for i=2:1:(n-1)
    for j=2:1:(n-1)
      x_n(idx(i, j)) = r*(x(idx(i-1, j)) + x(idx(i+1, j)) + x(idx(i, j-1)) + x(idx(i, j+1))) + (1-4*r)*x(idx(i, j));
    end
  end
  x = x_n;
end
toc()
output(x, t);
