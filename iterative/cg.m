clear

n=201;

x = zeros(1, n);
x(1) = 10;
x(n) = -8;

x_next = x;
  
# 係数行列A
A = speye(n, n);
r = 1;
for i=2:1:(n-1)
  A(i, i-1) = -r;
  A(i, i) = 2*r;
  A(i, i+1) = -r;
end

function eps=residual(r)
  eps = max(abs(r));
endfunction

tic();
b = x';
rand("seed", n);
x = rand(n, 1);
r = b - A*x;
rs = residual(r, x);
rr = r';

p = r;
maxB = max(b);
for it=1:1:n
  Ap = A*p;
  pAp = p'*Ap;
  alpha = (r'*p)/pAp;
  x = x + alpha*p;
  #r = r - alpha*Ap;
  
  r = b - A*x;
  rs = [rs, residual(r)];
  rr = [rr; r'];
  it++;
  
  beta = -(r'*Ap)/pAp;
  p = r + beta*p;
end
toc()

for i=1:2:20
  plot(0:1:(n-1), rr(i,:));
  axis([0,n-1, -10, 10]);
  filename = sprintf("residual_cg_%04d.png", i-1);
  print(filename, "-dpng");
  
  plot(abs(fft(rr(i,:))));
  axis([1,n/2, 0, 30]);
  filename = sprintf("residual_spector_cg_%04d.png", i-1);
  print(filename, "-dpng");
end

semilogy(0:1:(length(rs)-1), rs);
axis([0,n-1, 1e-8, 100]);
filename = sprintf("iterative_cg.png", i);
print(filename, "-dpng");

plot(0:1:(n-1), x);
filename = sprintf("x_cg.png", it);
axis([0,n-1, -10, 10]);
print(filename, "-dpng");